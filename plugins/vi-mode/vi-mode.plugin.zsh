fpath+="${0:h}"
ZVSC_EDITOR=nvr

# Updates editor information when the keymap changes.
function zle-keymap-select() {
	case $KEYMAP in
		# block
		vicmd) print -n "\e[2 q" ;;
		# bar
		*) print -n "\e[6 q" ;;
	esac
	zle reset-prompt
	zle -R
}

# sets keymap on next commandline
zle-line-init() {
	zle -K viins
}
zle -N zle-line-init

# Ensure that the prompt is redrawn when the terminal size changes.
TRAPWINCH() {
	zle &&  zle -R
}

zle -N zle-keymap-select
zle -N edit-command-line

bindkey -v

# allow :e to edit the command line
zle -A edit-command-line e

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey '^P' up-history
bindkey '^N' down-history

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word

# allow ctrl-r to perform backward search in history
bindkey '^r' history-incremental-search-backward

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line

# More text objects
autoload -Uz select-quoted select-bracketed
zle -N select-quoted
zle -N select-bracketed

for m in vicmd viopp; do
	for seq in {a,i}{\',\",\`}; do
		bindkey -M "$m" "$seq" select-quoted
	done
	for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
		bindkey -M $m $c select-bracketed
	done
done

# surround
autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -M visual S add-surround

# pipe a text selected by a motion through another command
autoload -Uz vi-pipe
zle -N vi-pipe
bindkey -a '!' vi-pipe

# toggle quotes around a motion
autoload -Uz vi-quote
zle -N vi-quote
zle -N vi-unquote vi-quote
bindkey -a 'Q' vi-unquote
bindkey -a 'q' vi-quote
