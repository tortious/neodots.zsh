fpath+="${0:h}"
autoload -Uz __flaunch fzf-launch-widget _grc tmax __tmux-sessions lines efn ewh hifn hial hiwh
zle -N fzf-launch-widget
zle -N efn
bindkey '^X' fzf-launch-widget
(( ${plugins[(I)zsh-autosuggestions]} )) && bindkey '^ ' autosuggest-accept
compdef __tmux-sessions tmax
compctl -F efn hifn
compctl -a hial
compctl -m ewh hiwh
