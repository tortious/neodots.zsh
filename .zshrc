# {{{ SSH multiplexing
if (( $+SSH_CONNECTION & ~$+TMUX_VERSION & ~$+SCREEN & ~$+DTACH )); then
	if (( $+commands[tmux] )); then
		target="$(tmux list-sessions -F "#S" 2>/dev/null)"
		if ! (( $? )) ; then
			session="$(tmux new-session -t "${target%%$'\n'*}")"
			[[ "$session" = \[detached* ]] && tmux kill-session -t "${session:24: -2}"
		else tmux new-session; fi
	elif (( $+commands[dtach] )); then
		export DTACH="$PREFIX/tmp/$USER-dtach"
		dtach -A "$DTACH" zsh
		unset DTACH
	fi
fi
# }}}
# {{{ Termux-specific
if (( ${+ANDROID_DATA} )); then
	:
else
	:
fi
# }}}
# {{{ P10k instant prompt
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# }}}
. $ZDOTDIR/.zprofile
# {{{ Interactive vars/options
HISTSIZE=50000
HISTFILE="$ZDOTDIR/zsh_history"
KEYTIMEOUT=20 # 200 ms
SAVEHIST=10000
eval "$(TERM=xterm dircolors -b)"

export BROWSER=xdg-open
export FZF_DEFAULT_OPTS="--bind=\"ctrl-j:preview-down,ctrl-k:preview-up,ctrl-alt-j:preview-page-down,ctrl-alt-k:preview-page-up,ctrl-a:select-all,ctrl-alt-a:deselect-all,alt-a:toggle-all,alt-e:execute($EDITOR {+})\""
export HISTORY_IGNORE="(#i)(cd|cd ..|clear|g ca#m *|l[sal]#( *)#|ldot|exit)"
export RIPGREP_CONFIG_PATH="$HOME/.config/rg.conf"
ZCALCPROMPT="%F{5}%1v%F{reset}:%F{11}λ%F{reset} "
ZSH_AUTOSUGGEST_USE_ASYNC="true"
ZSH_EVIL_SYNC_EDITOR="$EDITOR"

setopt autocd autopushd
setopt extended_history       # record timestamp of command in HISTFILE
setopt extendedglob globstarshort nullglob
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt promptsubst
setopt share_history          # share command history data
setopt cbases octalzeroes     # 0x[hex], 0[octal]
# }}}
# {{{ Zplugin
for dir in $gitrepopath; do
	if [[ -d $dir/zplugin ]]; then
		source $dir/zplugin/zplugin.zsh
		module_path+=( "$dir/zplugin/zmodules/Src" )
		zmodload zdharma/zplugin
		break
	fi
done

zplugin ice compile"*.lzui"
zplugin load zdharma/zui

zplugin ice depth=1
zplugin light romkatv/powerlevel10k
source $ZDOTDIR/.p10k.zsh

zplugin ice wait"1"
zplugin light MichaelAquilina/zsh-you-should-use

zplugin ice wait "0" ver"dev"
zplugin light zsh-vi-more/evil-registers

zplugin ice wait "0" ver"dev"
zplugin light zsh-vi-more/vi-increment

zplugin ice wait "0"
zplugin light $ZDOTDIR/plugins/vi-mode

zplugin ice wait "1"
zplugin light $ZDOTDIR/plugins/aliases

zplugin ice wait "0"
zplugin load $ZDOTDIR/plugins/colored-man-pages

zplugin ice wait"0"
zplugin light $ZDOTDIR/plugins/i3tool

zplugin ice wait"1"
zplugin load $ZDOTDIR/plugins/my

zplugin ice wait"1" blockf
zplugin light thetic/extract

zplugin ice wait"0"
zplugin snippet ${PREFIX:-/usr}/share/fzf/completion.zsh

zplugin ice wait"0"
zplugin snippet ${PREFIX:-/usr}/share/fzf/key-bindings.zsh

zplugin ice wait"0"
zplugin light zdharma/fast-syntax-highlighting

zplugin ice wayit"0"
zpl light zsh-users/zsh-autosuggestions
# }}}
# {{{ Completion
autoload -U zmv zcalc zargs
autoload -Uz compinit
compinit

# completion style
zmodload -i zsh/complist
bindkey -M menuselect '^o' accept-and-infer-next-history
zstyle ':completion:*:*:*:*:*' menu select
(( ${+LS_COLORS} )) || eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Autocomplete on hidden(dot) files
_comp_options+=(globdots)

# }}}
source $ZDOTDIR/zdirs

[[ -n "$startup" ]] && eval ${startup} || true
# something always screws with my tabs
tabs -4
# vim: set foldmethod=marker:
