# $PATH is overwritten in login shells, messing with tmux
typeset -U path
[ -d "$HOME/.local/bin" ] && path=($HOME/.local/bin $path[@])
[ -d "$HOME/.cargo/bin" ] && path=($HOME/.cargo/bin $path[@])
